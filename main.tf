provider "aws" {
  region = "us-west-2"

  # Make it faster by skipping something
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

##########################################
# Lambda Function (with various triggers)
##########################################

module "lambda_function" {
  source = "./modules/lambda"

  function_name = "${random_pet.this.id}-lambda-triggers"
  description   = "My awesome lambda function"
  handler       = "index.lambda_handler"
  runtime       = "python3.8"
  publish       = true

  create_package         = false
  local_existing_package = "${path.module}/modules/awslambdapythonec2.zip"

  allowed_triggers = {
    ScanAmiRule = {
      principal  = "events.amazonaws.com"
      source_arn = aws_cloudwatch_event_rule.every12hours.arn
    }
  }
}

##################
# Extra resources
##################

resource "random_pet" "this" {
  length = 2
}

##################################
# Cloudwatch Events (EventBridge)
##################################
resource "aws_cloudwatch_event_rule" "every12hours" {
  name          = "EC2CreateImageEvent"
  description   = "EC2 Create Image Event..."
  schedule_expression = "rate(12 hours)"
}

resource "aws_cloudwatch_event_target" "ec2statustags" {
  rule = aws_cloudwatch_event_rule.every12hours.name
  arn  = module.lambda_function.lambda_function_arn
}
resource "aws_lambda_permission" "allow_cloudwatch" {
    statement_id = "AllowExecutionFromCloudWatch"
    action = "lambda:InvokeFunction"
    function_name = "${random_pet.this.id}-lambda-triggers"
    principal = "events.amazonaws.com"
    source_arn = aws_cloudwatch_event_rule.every12hours.arn
}
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["sts:AssumeRole","ec2:Describe*","autoscaling:Describe*","s3:*","cloudwatch:ListMetrics","cloudwatch:GetMetricStatistics","cloudwatch:Describe*","elasticloadbalancing:Describe*","logs:CreateLogGroup","logs:CreateLogStream","logs:PutLogEvents"]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}
resource "aws_lambda_permission" "with_sns" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = "${random_pet.this.id}-lambda-triggers"
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.default.arn
}

resource "aws_sns_topic" "default" {
  name = "call-lambda-maybe"
}

resource "aws_sns_topic_subscription" "lambda" {
  topic_arn = aws_sns_topic.default.arn
  protocol  = "lambda"
  endpoint  = "${random_pet.this.id}-lambda-triggers.arn"
}
resource "aws_iam_role" "default" {
  name = "iam_for_lambda_with_sns"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}