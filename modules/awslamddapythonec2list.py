import json
import boto3
import requests

ec2 = boto3.resource('ec2')
s3 = boto3.resource('s3')
ec2client = boto3.client('ec2')
url = 'http://aws_vpc_endpoint.amazonaws.com/validtags/Service'
validtags = requests.get(url)


def lambda_handler(event, context):
    Test = event["Test"]
    filters = [
        {
            'Name': 'instance-state-name',
            'Values': ['running']
        }
    ]
    instances = ec2.instances.filter(Filters=filters)
    RunningInstances = []
    for instance in instances:
        response = ec2client.describe_instances(InstanceIds=[instance])
        RunningInstances.append(response)
        instancelist = json.dumps(RunningInstances)
        s3.Object('trimble-com-aws', 'instancelist.csv').put(Body=instancelist)

    reservations = ec2.describe_instances().get('Reservations', [])
    for reservation in reservations:
        for instance in reservation['Instances']:
            tags = {}
            for tag in instance['Tags']:
                tags[tag['Key']] = tag['Value']
                if ('Service' not in tags) and (Test is not True):
                    notification = instance['InstanceId'] + "does not have Service Tag"
                    client = boto3.client('sns')
                    response = client.publish(
                        TargetArn="<ARN of the SNS topic>",
                        Message=json.dumps({'default': notification}),
                        MessageStructure='json'
                    )

                elif (tags['Service'] not in ['Data', 'Processing', 'Web']) and (Test is not True):
                    tagname = event.get('Service')
                    notification = instance['InstanceId'] + "has Unknown Owner tag. Valid tags are" + tagname
                    client = boto3.client('sns')
                    response = client.publish(
                        TargetArn="<ARN of the SNS topic>",
                        Message=json.dumps({'default': notification}),
                        MessageStructure='json'
                    )
                else:
                    (print("All instances have valid tags"))
    return {
        "statusCode": 200
    }
